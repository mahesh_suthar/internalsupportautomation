from httplib2 import Http
import json


class Chatbot:

    @staticmethod
    def chatbot(url, msg):
        if msg is None:
            msg = {
                "text": "No Message to show",
            }

        url = url

        message_headers = {'Content-Type': 'application/json; charset=UTF-8'}

        http_obj = Http()

        http_obj.request(
            uri=url,
            method='POST',
            headers=message_headers,
            body=json.dumps({"text": str(msg)})
        )


if __name__ == '__main__':
    chat = Chatbot()
