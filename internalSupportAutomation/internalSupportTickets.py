from zenpy import Zenpy
from jira import JIRA
import requests
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib
from requests.auth import HTTPBasicAuth
import re
import json
from zenpy.lib.api_objects import CustomField, Comment
from jira.exceptions import JIRAError
from chatbot import Chatbot
import datetime
import os
import sys
import ast
import pandas as pd
import gspread
from oauth2client.service_account import ServiceAccountCredentials

sys.stdout = open("/home/mahesh/logfile/internal_support.log", "a")

scope = ["https://spreadsheets.google.com/feeds", "https://www.googleapis.com/auth/drive"]


# Rahul Jogi: dT5jhbg3YXhe8zooAGap4CC5
# Rohan gandhi: d4vHaYrl1vX4PW2QQT4E4907
# Pooja Datrange: 9HLybhjt675Qe9BSgw0J8451
# Rutuja: 26vNv0ewQuS4aoVY0GwiB586


class InternalSupport:
    def __init__(self):
        creds = {
            'email': 'mahesh.suthar@vuclip.com',
            'token': 'oM1XqkOhzlzzBDbD6tqeURUE0M0Ge93xYZe3UrJ6',
            'subdomain': 'vuclip'
        }

        self.zenpy_client = Zenpy(**creds)
        self.url = 'https://chat.googleapis.com/v1/spaces/AAAAa1kgsSY/messages?key=AIzaSyDdI0hCZtE6vySjMm' \
                   '-WEfRq3CPzqKqqsHI&token=MNLY4GKu3mUH6qUQy13fvwIo12BnkqiTMytMcSbqj2Y%3D'
        # self.country_path = os.path.abspath("vuclip_ticket-field_Country.csv")
        # self.categ_path = os.path.abspath("vuclip_ticket-field_Category.csv")
        # self.config_path = os.path.abspath("ops-poc-859dfa45aba4.json")
        self.country_path = "/home/mahesh/internalsupportautomation/internalSupportAutomation/vuclip_ticket" \
                            "-field_Country.csv"
        self.categ_path = "/home/mahesh/internalsupportautomation/internalSupportAutomation/vuclip_ticket" \
                          "-field_Category.csv"
        self.config_path = "/home/mahesh/internalsupportautomation/internalSupportAutomation/ops-poc-859dfa45aba4.json"
        self.internal_google_sheet_url = "https://docs.google.com/spreadsheets/d/1ncSPEbGV7Mgx0KAH6bYFhOR7lL2lGTHhcmz0pX720YU/edit?ts=60128f03#gid=0"

    # Get All the required details from zendesk.
    # Use jira function to create the Jira using that Jira.

    def google_sheet_client(self):
        gcreds = ServiceAccountCredentials.from_json_keyfile_name(
            self.config_path, scope)
        gclient = gspread.authorize(gcreds)
        return gclient

    def update_google_sheet(self, ticket_id, country, priority, team, requester, jiraid):
        print("Update Google Sheet: Start")
        current_date = datetime.date.today()
        current_month_sheet = datetime.date.strftime(datetime.date.today(), "%B %Y")
        client_obj = self.google_sheet_client()
        sheet = client_obj.open_by_url(self.internal_google_sheet_url)
        try:
            sh = sheet.worksheet(current_month_sheet)
        except gspread.exceptions.WorksheetNotFound:
            sh = sheet.get_worksheet(0)
            sheet_list = sheet.worksheets()
            max_sheet_id = sheet_list.pop().id
            sh = sh.duplicate(max_sheet_id, max_sheet_id + 1, current_month_sheet)
            sh.resize(rows=1)
            sh.resize(rows=1000)
        try:
            sh.find(str(ticket_id))
            print("Entry Already Exist")
        except gspread.exceptions.CellNotFound:
            ticket = self.zenpy_client.tickets(id=ticket_id)
            sub = ast.literal_eval(json.dumps(ticket.subject))
            l2_user = ticket.assignee.name
            data = sh.get_all_values()
            index_value = len(data)
            row_data = [index_value, str(current_date), ticket_id, sub, l2_user,
                        country, priority, team, requester, jiraid]
            print(row_data)
            sh.append_row(row_data)
        print("Updated Google Sheet: End")

    def get_ticket_id(self, t_id):
        ticket = self.zenpy_client.tickets(id=t_id)
        return ticket

    def get_zendesk_categ(self, team):
        df = pd.read_csv(self.categ_path)
        zendesk_tag = ''
        for index, row in df.iterrows():
            if row['Support Tag'] == team:
                zendesk_tag = row['Zendesk Tag']
                break
        return zendesk_tag

    def get_zendesk_country(self, country):
        df = pd.read_csv(self.country_path)
        zendesk_key = ''
        for index, row in df.iterrows():
            if row['Support Key'] == country:
                zendesk_key = row['Zendesk Key']
                break
        return zendesk_key

    @staticmethod
    def send_mail_to_requester(requester, ticket_id, jiraid):
        user_name = requester.split('@')
        comment = 'Hi ' + user_name[0] + ', \n\n Your internal ticket is created. \n ' \
                  + 'We are working on your request and the Jira is https://vuclipdev.atlassian.net/browse/' \
                  + str(jiraid) + ".\nWe will get back to you as soon as possible.\n\nThanks,\nVIU Support"
        msg = MIMEMultipart()
        msg['From'] = 'mahesh.suthar@vuclip.com'
        msg['To'] = requester
        rcpt = [requester]
        msg['Subject'] = 'New Internal Ticket: ' + str(ticket_id)
        msg.attach(MIMEText(comment, 'plain'))
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.starttls()
        server.login("mahesh.suthar@vuclip.com", "Chintu@1990")
        text = msg.as_string()
        server.sendmail("mahesh.suthar@vuclip.com", rcpt, text)
        server.quit()

    def get_details_from_zendesk(self):
        print("Script Start:: " + str(datetime.datetime.now()))
        jira_assignee = {'content': 'amol.dudhe',  # amol.dudhe
                         'cms': 'amol.dudhe',    # amol.dudhe
                         'sre': 'rutuja.chinchkhede',
                         'application': 'rutuja.chinchkhede',
                         'backend': 'mahesh.suthar',
                         'billing': 'rahul.jogi',
                         'price point': 'uttara.gurav',
                         'insight': 'samadhan.paigavan',  # 'samadhan.paigavan',
                         'insights': 'samadhan.paigavan',  # 'samadhan.paigavan',
                         'browser': 'amol.dudhe', # amol.dudhe
                         'tv': 'amol.dudhe', # amol.dudhe
                         'team not known': 'uttara.gurav'}

        zendesk_assignee = {'rutuja.chinchkhede': 361080170273,
                            'rahul.jogi': 372121455054, 'ganesh.patil': 395997708013, 'mahesh.suthar': 361081729614,
                             'uttara.gurav': 361080162533, 'amol.dudhe': 360544107753, 'samadhan.paigavan': 361929869574}
        # 'sanjay.madankar': 360544107753 Leave the office from 17th june 22

        priorities = {'P0': 'Blocker', 'P1': 'Critical', 'P2': 'Major', 'P3': 'Minor'}

        chat = Chatbot()
        zendesk_tickets = self.zenpy_client.search(type='ticket', status='new', group_id=360001242713, assignee_id=None)
        for ticket in zendesk_tickets:
            count = 0
            already_marked = False
            for comment in self.zenpy_client.tickets.comments(ticket=ticket.id):
                if count > 0 and 'https://vuclipdev.atlassian.net/browse' in comment.body:
                    already_marked = True
                    break
                count += 1
            if already_marked:
                print("Avoid Duplicate")
                continue
            print("Pick A New Ticket: ", ticket)
            desc = ticket.description
            data_list = desc.split('\n')
            print(data_list)
            team, priority, reporter, country, category, subcategory = '', '', '', '', '', ''
            ticket_summary = ticket.subject
            status = ticket.status
            assignee = ticket.assignee_id
            for data in data_list:
                if not team and re.search("Team:", data):
                    team = data.split(':')[1].strip()
                elif not team and re.search("Category:", data):
                    team = data.split(':')[1].strip()
                    print("team:  ", team)
                elif not priority and re.search("Priority:", data):
                    priority = data.split(':')[1].strip()
                elif not reporter and re.search("Requester:", data):
                    reporter = data.split(':')[1].strip()
                elif not country and re.search("Country:", data):
                    country = data.split(':')[1].strip()
                # if not category and re.search("Category:", data):
                #     if data.split(':')[0] == 'Category':
                #         category = data.split(':')[1].strip()
                if re.search("Sub-category:", data):
                    subcategory = data.split(':')[1].strip()

            print(status, assignee, team, priority, reporter, country, category, subcategory)
            priority = priorities[priority[0:2]]
            if country:
                country = self.get_zendesk_country(country)
            if team:
                category = self.get_zendesk_categ(team)
            print("testing", country, category,team, reporter)

            if team and reporter:
                count += 1
                watcher = reporter.split('@')
                watcher = watcher[0]  # Get watcher from reporter
                normalize_team = team.lower()
                # Create a Jira ticket based on details provided in Zendesk ticket

                if normalize_team in jira_assignee.keys():
                    print("Assignee Available and Creating Jira For : ", normalize_team)
                    # Get assignee_id from Dict
                    if subcategory in ['Clear identity', 'Promo code', 'Promo code Creation']:
                        z_assignee = 'samadhan.paigavan'
                    else:
                        z_assignee = jira_assignee[normalize_team]
                    get_jiraid = self.create_jira(ticket.id, ticket_summary, priority, desc, z_assignee, watcher)

                    # Update the ticket with generalize comment
                    self.zendesk_ticket_update(ticket, country, category, zendesk_assignee[z_assignee], get_jiraid,
                                               watcher)
                    chat.chatbot(self.url, "Team: " + str(team) + "\nAssignee: " +
                                 str(z_assignee) + "\nJiraId: " + str(get_jiraid) + "\nPriority: " + str(priority))
                    self.update_google_sheet(ticket.id, country, priority, team, watcher, str(get_jiraid))
            else:
                self.zendesk_error_comment(ticket, country, category, 372121455054)
                chat.chatbot(self.url, str(ticket.id) + " not commented some issue with Automation")
            print(count)
        print("Script Close:: " + str(datetime.datetime.now()))

    def upload_attachment_in_jira(self, ticket_id, jira_obj, issue_id):
        try:
            # path = '/home/mahesh/Desktop/attachment/'
            # path = '/Users/maheshsuthar/mahesh/vuclip_support/attachment/'
            path = '/home/mahesh/internalsupportautomation/jira_attachment/'
            comments = self.zenpy_client.tickets.comments(ticket=ticket_id)
            for comment in comments:
                for attachment in comment.attachments:
                    print(attachment.id)
                    self.zenpy_client.attachments.download(attachment.id, path)

            attachment_list = os.listdir(path)
            if attachment_list:
                for attach in attachment_list:
                    jira_obj.add_attachment(issue=issue_id, attachment=path + attach)
                for att in attachment_list:
                    if os.path.isfile(path + att):
                        os.remove(path + att)
            return True
        except Exception as e:
            print("Exception On upload Attachment: ", e)
            return False

    def create_jira(self, ticketid, summary, priority, desc, reporter, watcher):
        jiraid = ''
        try:
            viu_jira = {'server': 'https://vuclipdev.atlassian.net'}  # jira server URL

            jira_users = {'rutuja.chinchkhede': '26vNv0ewQuS4aoVY0GwiB586',
                          'rahul.jogi': 'dT5jhbg3YXhe8zooAGap4CC5',
                          'ganesh.patil': '2VBzx0yQ5lcp3zcADikX945D',
                          # 'sanjay.madankar': 'kyn4Tywb94PvTMtWGudC3646',  X-Employee
                          'amol.dudhe': '9zY5FIcZDec2TxvXcRCAE758',
                          'samadhan.paigavan': 'vEHPTdwEBVpb8UiNCDMD73C9',
                          'uttara.gurav': '8qIOut8clidlCPRTniig4C93',
                          'mahesh.suthar': 'mO3KGfX8KvcCFGNRqwD797B6'}
            jira_user_account_id = {'rutuja.chinchkhede': '5c3c23dad254b55db84d4494',
                                    'rahul.jogi': '5c3c23da7d0c1a2f011270a2',
                                    'ganesh.patil': '5dbbca6108ee050c31803081',
                                    # 'sanjay.madankar': '6091267733a9410071c35077',  X-Employee
                                    'amol.dudhe': '62f3756b3cc20c06c8b1034e',
                                    'samadhan.paigavan': '63231cbd978dae24dc0756db',
                                    'uttara.gurav': '5cb5969d77da6111cfd2d494',
                                    'mahesh.suthar': '5d807eed8a50e80c2fee8278'}

            jira_user = reporter + '@vuclip.com'
            jira_password = jira_users[reporter]
            user_account_id = jira_user_account_id.get(reporter)
            jira = JIRA(basic_auth=(str(jira_user), str(jira_password)),
                        options=viu_jira)  # Jira connection object
            summary = summary + " - ZendeskId: " + str(ticketid)
            print("In  create_jira function: ")
            jiraid = jira.create_issue(project='VIUL2', summary=str(summary),
                                       description=desc, issuetype={'name': 'Support Ticket'},
                                       assignee={'id': user_account_id},
                                       labels=['zendesk_esclation'],
                                       priority={'name': priority})  # Insup added by Mahesh
            print("In  create_jira function Created Successfully: ", jiraid)
            self.upload_attachment_in_jira(ticketid, jira, jiraid)
            ids = self.find_account_id(watcher, jira_user, jira_password)
            if ids:
                jira.add_watcher(jiraid, str(ids))
            return jiraid
        except JIRAError:
            print("Jira Error")
            return jiraid

    @staticmethod
    def find_account_id(watcher, jira_user, password):
        try:
            splitted_name = watcher.split('.')
            if splitted_name:
                watcher_name = splitted_name[0] + ' ' + splitted_name[1]
            else:
                watcher_name = splitted_name
            url = "https://vuclipdev.atlassian.net/rest/api/3/users/search"
            auth = HTTPBasicAuth(jira_user, password)
            headers = {
                "Accept": "application/json"
            }
            result = []
            start = 0
            max_result = True
            while max_result:
                data = {
                    'startAt': start,
                    'maxResults': 25000
                }
                response = requests.request(
                    "GET",
                    url,
                    headers=headers,
                    params=data,
                    auth=auth
                )
                start += 1000
                response = json.loads(response.text)
                # print(type(response), len(response))
                result.extend(response)
                if len(response) == 1000:
                    max_result = True
                else:
                    max_result = False

            print(len(result))
            for res in result:
                # print str(res.get('displayName')).lower()
                # print str(res.get('accountId'))
                if str(res.get('displayName')).lower() == watcher_name:
                    if res.get('accountId'):
                        return res.get('accountId')
                    break
        except Exception as e:
            return ''

    def zendesk_ticket_update(self, t_id, country, category, assignee, jiraid, watcher):
        comment = "Hi " + str(watcher) + ",\n\nWe are working on your request and the Jira is " \
                                         "https://vuclipdev.atlassian.net/browse/" + str(jiraid) + ".\nWe will get " \
                                                                                                   "back to you as " \
                                                                                                   "soon as " \
                                                                                                   "possible.\n" \
                                                                                                   "\nThanks," \
                                                                                                   "\nVIU Support "
        tags = t_id.tags
        tags.append('jira_escalated')
        t_id.custom_fields.append(CustomField(id=360002732474, value="email"))
        t_id.custom_fields.append(CustomField(id=360002574154, value=country))
        t_id.custom_fields.append(CustomField(id=360002726973, value=category))
        t_id.custom_fields.append(CustomField(id=4416688140185, value=str(jiraid)))
        t_id.__setattr__('assignee_id', assignee)
        t_id.__setattr__('group_id', 360001242713)
        t_id.__setattr__('tags', tags)
        t_id.__setattr__('status', 'open')
        t_id.comment = Comment(body=comment, public=True)
        self.zenpy_client.tickets.update(t_id)

    def zendesk_error_comment(self, t_id, country, category, assignee):
        comment = "Hi" + "\n\nPlease create ticket using internalsupport portal.\nUrl address is: " \
                         "'http://internalsupport.vuclip.com'. \nThanks,\nVIU Support "
        t_id.custom_fields.append(CustomField(id=360002732474, value="email"))
        t_id.custom_fields.append(CustomField(id=360002574154, value=country))
        t_id.custom_fields.append(CustomField(id=360002726973, value=category))
        t_id.__setattr__('assignee_id', assignee)
        t_id.__setattr__('group_id', 360001242713)
        t_id.__setattr__('tags', ['jira_esclated'])
        t_id.__setattr__('status', 'pending')
        t_id.comment = Comment(body=comment, public=True)
        self.zenpy_client.tickets.update(t_id)


if __name__ == '__main__':
    client = InternalSupport()
    client.get_details_from_zendesk()
    # client.update_google_sheet(1516104, "Dubai", "Blocker", "Content", "mohamed.refay", "VIUL2-9522")
